
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 //to dela
function generirajPodatke(stPacienta) {
     
     
  //for (var stPacienta = 1; stPacienta <= 3; stPacienta++) {
   // var ehrId = "";
  
    if (stPacienta == 1) {
      var ime = "Goran";
      var priimek = "Dragić";
      var datumRojstva = "1986-05-06"+"T00:00:00.000Z"; 
    }    
    else if (stPacienta == 2) {
      var ime = "Janez";
      var priimek = "Novak";
      var datumRojstva = "1993-02-24"+"T00:00:00.000Z";
    } 
    else if (stPacienta ==3){
      var ime = "Jonah";
      var priimek = "Hill";
      var datumRojstva = "1983-12-20"+"T00:00:00.000Z";
    }
    
  // Kreira EHRid
  
	  
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
     //   $("#kreirajSporocilo").append("EHR: " + ehrId);
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              
           //   $("#preberiObstojeciEHR")[0][stPacienta].val =ehrId;
              
              
              switch (stPacienta) {
                case 1:
                  $("#GD").val(ehrId); 
                  $("#dodajMeritveGD").val(ehrId);
                  $("#pregledMeritveGD").val(ehrId);
                  break;
                  
                case 2:
                  $("#JN").val(ehrId);
                  $("#dodajMeritveJN").val(ehrId);
                  $("#pregledMeritveJN").val(ehrId);
                  break;
                
                case 3:
                  $("#JH").val(ehrId);
                  $("#dodajMeritveJH").val(ehrId);
                  $("#pregledMeritveJH").val(ehrId);
                  break;
              }
            
            if(stPacienta==1){
              dodajPodatkeGeneriranje(ehrId, "194.4", "84.9", "2009-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "194.6", "85.0", "2011-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "194.5", "84.3", "2013-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "194.4", "85.2", "2014-05-20T09:30");
              $("#dodajMeritveGD").val(ehrId + "|2015-05-20T09:30|194.4|84.7");
            }

            else if(stPacienta==2){
              dodajPodatkeGeneriranje(ehrId, "180.5", "50.2", "2008-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "180.5", "50.3", "2010-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "180.4", "50.5", "2012-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "180.4", "50.2", "2013-04-20T09:30");
              $("#dodajMeritveJN").val(ehrId + "|2015-05-20T09:30|180.5|50.7");
            }

            else if(stPacienta==3){
              dodajPodatkeGeneriranje(ehrId, "170.4", "84.2", "2014-05-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "170.5", "84.5", "2015-02-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "170.4", "84.0", "2015-08-20T09:30");
              dodajPodatkeGeneriranje(ehrId, "170.3", "84.9", "2016-02-20T09:30");
              $("#dodajMeritveJH").val(ehrId + "|2017-05-20T09:30|170.4|84.5");
            } 
            
            }
          },
          
          
          error: function(err) 
          {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
          
        });
      }
		});
		
  //}
}
// to dela
function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              $("#dodajVitalnoEHR").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
 
 // master/detail vzorec
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

// to dela
function dodajPodatkeGeneriranje(ehrId, telesnaVisina, telesnaTeza, datumInUra) {

		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {},
      
      error: function(err) {
      	$("#dodajPodatkeSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}

// to dela
function dodajPodatke() {
  
  var ehrId = $("#dodajVitalnoEHR").val();
  var datumInUra = $("#dodajVitalnoDatumInUra").val();
  var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajPodatkeSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajPodatkeSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>Podatki uspešno dodani.</span>"); //res.meta.href
          document.getElementById("bmiGumb").disabled = false;
      },
      
      
      error: function(err) {
      	$("#dodajPodatkeSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

// to dela
// izračuna bmi 
function izracunajBMI() {

  var ehrId = $("#dodajVitalnoEHR").val();
  var visina;
  var teza;
  var BMI;
  
  $.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  	type: 'GET',
  	headers: {
      "Authorization": getAuthorization()
    }
  });
  
	$.ajax({
	  url: baseUrl + "/view/" + ehrId + "/" + "height",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) 
    {
      for (var i in res) 
         visina = res[0].height;

    	$.ajax({
    	  url: baseUrl + "/view/" + ehrId + "/" + "weight",
        type: 'GET',
        headers: {
          "Authorization": getAuthorization()
        },
        success: function (res) 
        {
          for (var i in res) 
            teza = res[0].weight;
          
          BMI = Math.round((teza / Math.pow(visina, 2) * 10000) * 100) / 100;
          $("#bmiRezultat").val(BMI);
          return BMI; 
        }
      });
    }
  });
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */
 // to dela
function preberiMeritveVitalnihZnakov() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiDodajSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
            "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
            " " + party.lastNames + "'</b>.</span><br/><br/>");
            
  				if (tip == "telesna višina")
  				{
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) 
    			    {
    			    	if (res.length > 0) 
    			    	{
              	  var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna višina</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height + " " 	+
                      res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
  				else if (tip == "telesna teža") 
  				  {
  					$.ajax({
    			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results =  "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna višina</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() 
    			    {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				}
  				
	    	},
	    	
	    	error: function(err) 
	    	{
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajPodatkeSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});
